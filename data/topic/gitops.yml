title: What is GitOps?
header_body: >-
  GitOps is an operational framework that takes DevOps best practices used for
  application development such as version control, collaboration, compliance,
  and CI/CD, and applies them to infrastructure automation.



  [Watch our GitOps webcast →](/why/gitops-infrastructure-automation/){:data-ga-name="GitOps webcast"}{:data-ga-location="header"}
description: GitOps is a process of automating IT infrastructure using
  infrastructure as code and software development best practices such as Git,
  code review, and CI/CD pipelines.
canonical_path: /topics/gitops/
file_name: gitops
twitter_image: /images/opengraph/iac-gitops.png
cover_image: /images/topics/gitops-header.png
body: >-
  ## What is GitOps?


  GitOps is an [operational](/topics/ops/){:data-ga-name="Operations"}{:data-ga-location="body"} framework that takes DevOps best practices used for application development such as version control, collaboration, compliance, and CI/CD tooling, and applies them to infrastructure automation. While the software development lifecycle has been automated, infrastructure has remained a largely manual process that requires specialized teams. With the demands made on today’s infrastructure, it has become increasingly crucial to implement infrastructure automation. Modern infrastructure needs to be elastic so that it can effectively manage cloud resources that are needed for continuous deployments.


  Modern applications are developed with speed and scale in mind. Organizations with a mature DevOps culture can deploy code to production hundreds of times per day. DevOps teams can accomplish this through development best practices such as version control, code review, and CI/CD pipelines that automate testing and deployments.


  GitOps is used to automate the process of provisioning infrastructure. Similar to how teams use application source code, operations teams that adopt GitOps use configuration files stored as code (infrastructure as code). GitOps configuration files generate the same infrastructure environment every time it’s deployed, just as application source code generates the same application binaries every time it’s built.


  ## How do teams put GitOps into practice?


  GitOps is not a single product, plugin, or platform. GitOps workflows help teams manage IT infrastructure through processes they already use in application development. 


  GitOps requires three core components:


  > **GitOps** = IaC + MRs + CI/CD


  **IaC**: GitOps uses a [Git repository](/blog/2020/11/12/migrating-your-version-control-to-git/){:data-ga-name="Git repository"}{:data-ga-location="body"} as the single source of truth for infrastructure definitions. Git is an open source version control system that tracks code management changes, and a Git repository is a .git folder in a project that tracks all changes made to files in a project over time. [Infrastructure as code (IaC)](/topics/gitops/infrastructure-as-code/){:data-ga-name="IaC"}{:data-ga-location="body"} is the practice of keeping all infrastructure configuration stored as code. The actual desired state may or may not be not stored as code (e.g., number of replicas or pods).


  **MRs**: GitOps uses merge requests (MRs) as the [change mechanism](/blog/2020/10/13/merge-request-reviewers/){:data-ga-name="Change mechanism"}{:data-ga-location="body"} for all infrastructure updates. The MR is where teams can collaborate via reviews and comments and where formal approvals take place. A merge commits to your main (or trunk) branch and serves as an audit log.


  **CI/CD**: GitOps automates infrastructure updates using a Git workflow with [continuous integration (CI)](/stages-devops-lifecycle/continuous-integration/) and continuous delivery (CI/CD). When new code is merged, the CI/CD pipeline enacts the change in the environment. Any configuration drift, such as manual changes or errors, is overwritten by GitOps automation so the environment converges on the desired state defined in Git. GitLab uses CI/CD [pipelines](/blog/2020/12/02/pre-filled-variables-feature/) to manage and implement GitOps automation, but other forms of automation, such as definitions operators, can be used as well.

  <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/JtZfnrwOOAw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
benefits_title: null
benefits: []
cta_banner:
  - title: GitOps challenges
    body: >-
      With any collaborative effort, change can be tricky and GitOps is no
      exception. GitOps is a process change that will require discipline from
      all participants and a commitment to doing things in a new way. It is
      vital for teams to write everything down.


      GitOps allows for greater [collaboration](/blog/2020/11/23/collaboration-communication-best-practices/){:data-ga-name="Collaboration"}{:data-ga-location="body"}, but that is not necessarily something that comes naturally for some individuals or organizations. A GitOps approval process means that developers make changes to the code, create a merge request, an approver merges these changes, and the change is deployed. This sequence introduces a “change by committee” element to infrastructure, which can seem tedious and time-consuming to engineers used to making quick, manual changes.


      It is important for everyone on the team to record what’s going on in merge requests and issues. The temptation to edit something directly in production or change something manually is going to be difficult to suppress, but the less “cowboy engineering” there is, the better GitOps will work.
  - title: What makes GitOps work?
    body: >-
      As with any emerging technology term, GitOps isn’t strictly defined the
      same way by everyone across the industry. GitOps principles can be applied
      to all types of infrastructure automation including VMs and containers,
      and can be very effective for teams looking to manage Kubernetes-based
      infrastructure.


      While many tools and methodologies promise faster deployment and seamless management between code and infrastructure, GitOps differs by focusing on a developer-centric experience. Infrastructure management through GitOps happens in the same version control system as the application development, enabling teams to collaborate more in a central location while benefiting from Git’s <a href="https://devops.com/an-inside-look-at-gitops/" target="_blank">[built-in features](https://devops.com/an-inside-look-at-gitops/)</a>.
resources_title: GitOps next step
resources_intro: >-
  Ready to learn more about GitOps? Here are a few resources to help you get
  started on your journey.

resources:
  - title: "Learn how GitLab streamlines GitOps workflows"
    url: /solutions/gitops/
    type: Next Steps
  - title: "What does infrastructure as code mean?"
    url: /topics/gitops/infrastructure-as-code/
    type: Next Steps
  - title: "Why GitLab’s collaboration technology is critical for GitOps"
    url: /topics/gitops/gitops-gitlab-collaboration/
    type: Next Steps
  - title: "How teams use GitLab and Terraform for infrastructure as code"
    url: /topics/gitops/gitlab-enables-infrastructure-as-code/
    type: Next Steps
  - title: "Multicloud deployment for GitOps using GitLab"
    url: /topics/gitops/gitops-multicloud-deployments-gitlab/
    type: Next Steps
  - title: "The benefits of GitOps workflows"
    url: /topics/gitops/gitops-best-practices/
    type: Next Steps
  - title: "What is a GitOps workflow?"
    url: /topics/gitops/gitops-workflow/
    type: Next Steps
  - title: "[Expert Panel Discussion] GitOps: The Future of Infrastructure Automation"
    url: /why/gitops-infrastructure-automation/
    type: Webcast
  - title: Managing infrastructure through GitOps with GitLab and Anthos
    url: https://about.gitlab.com/webcast/gitops-gitlab-anthos/
    type: Webcast
  - title:
      GitLab and HashiCorp - A holistic guide to GitOps and the Cloud Operating
      Model
    url: /webcast/gitlab-hashicorp-gitops/
    type: Webcast
  - title: Automating cloud infrastructure with GitLab and Terraform
    url: /webcast/gitops-gitlab-terraform/
    type: Webcast
  - title: GitOps with Pulumi and GitLab
    url: /webcast/gitops-gitlab-pulumi/
    type: Webcast
  - title: GitOps with AWS and GitLab
    url: /webcast/gitops-gitlab-aws-helecloud/
    type: Webcast
  - url: /webcast/digital-transformation-northwestern-mutual/
    title: Accelerating Digital Transformation at Northwestern Mutual
    type: Case studies
  - title: A beginner's guide to GitOps
    url: https://page.gitlab.com/resources-ebook-beginner-guide-gitops.html
    type: Books
  - url: https://youtu.be/JtZfnrwOOAw?list=PL05JrBw4t0KoixDjLVMzKwVgx0eNyDOIo
    type: Video
    title: What is GitOps? Why is it important? How can you get started?
  - url: https://www.youtube.com/watch?list=PLFGfElNsQthbno2laLgxeWLla48TpF8Kz&v=5ykRuaZvY-E&feature=emb_title
    type: Video
    title: Using GitLab for GitOps to break down silos and encourage collaboration
suggested_content:
  - url: /blog/2019/07/01/using-ansible-and-gitlab-as-infrastructure-for-code/
  - url: /topics/gitops/gitops-gitlab-collaboration/
  - url: /topics/gitops/gitlab-enables-infrastructure-as-code/
  - url: /topics/gitops/gitops-multicloud-deployments-gitlab/
  - url: /blog/2019/10/28/optimize-gitops-workflow/
  - url: /blog/2020/04/17/why-gitops-should-be-workflow-of-choice/
schema_faq:
  - question: What is GitOps?
    answer:
      GitOps is an operational framework that takes DevOps best practices used
      for application development such as version control, collaboration,
      compliance, and CI/CD, and applies them to infrastructure automation.
    cta:
      - url: https://about.gitlab.com/topics/gitops/#what-is-gitops:~:text=%E2%86%92-,What%20is%20GitOps
        text: Learn more about GitOps
  - question: Why choose GitLab for GitOps?
    answer:
      GitLab is a DevOps platform with built-in agile planning, source code
      management, and CI/CD delivered as a single application. GitLab also has
      tight integrations with the industry’s best GitOps tools and cloud
      platforms.
    cta:
      - text: Learn how GitLab can help you adopt GitOps.
        url: https://about.gitlab.com/solutions/gitops/
